package itca.agribank.demoJenkins;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class AgribankApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(AgribankApplication.class, args);
	}

}
