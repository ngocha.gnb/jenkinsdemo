package itca.agribank.demoJenkins.restapi;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/jenkins/vbaapi")
public class RestAPI {

    @GetMapping("/testapi")
    public String testAPI() {
        return "This is function to test new API for Jenkins CI/CD";
    }

    @GetMapping("/testwebhookv1")
    public String testWebhookv1() {
        return "This is function to test webhook for Jenkins CI/CD ver 1";
    }
}
